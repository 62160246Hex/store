/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.hex.store.poc.Utils;
import database.Bridge;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Product;

/**
 *
 * @author Code.Addict
 */
public class ProductDao implements DaoInterface<Product> {

	@Override
	public int add(Product object) {
		Connection bridge = Bridge.createBridge();

		try {
			String queryStr = "INSERT INTO product(name, price) VALUES(?, ?)";
			PreparedStatement stmt = bridge.prepareStatement(queryStr);
			stmt.setString(1, object.getName());
			stmt.setDouble(2, object.getPrice());
			int row = stmt.executeUpdate();
			ResultSet data = stmt.getGeneratedKeys();
			if (data.next()) {
				System.out.printf("[ Updated Product ] RowChanged(%d)\nId(%d)\n", row, data.getInt(1));
			}
			return data.getInt(1);
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}

		Bridge.closeBridge();
		return 0;
	}

	@Override
	public ArrayList<Product> getAll() {

		ArrayList list = new ArrayList();
		Connection bridge = Bridge.createBridge();
		try {

			String queryStr = "SELECT * FROM product";
			Statement stmt = bridge.createStatement();
			ResultSet data = stmt.executeQuery(queryStr);
			while (data.next()) {
				int id = data.getInt("id");
				String name = data.getString("name");
				double price = data.getDouble("price");
				list.add(new Product(id, name, price));
			}
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}
		Bridge.closeBridge();

		return list;
	}

	@Override
	public Product get(int id) {
		Connection bridge = Bridge.createBridge();
		Product result = null;
		try {

			String queryStr = "SELECT * FROM product WHERE id = " + id;
			Statement stmt = bridge.createStatement();
			ResultSet data = stmt.executeQuery(queryStr);
			result = new Product(data.getInt("id"), data.getString("name"), data.getDouble("price"));
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}
		Bridge.closeBridge();

		return result;
	}

	@Override
	public void delete(int id) {
		Connection bridge = Bridge.createBridge();
		try {
			String queryStr = "DELETE FROM product WHERE id = " + id;
			Statement stmt = bridge.createStatement();
			int rowChanged = stmt.executeUpdate(queryStr);
			Utils.print("[ Deleted Item Product ] " + rowChanged);
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}
		Bridge.closeBridge();
	}

	@Override
	public int update(Product object) {
		Connection bridge = Bridge.createBridge();

		/* Process */
		try {
			String queryStr = "UPDATE product SET name = ?, price = ? WHERE id = ?";
			PreparedStatement stmt = bridge.prepareStatement(queryStr);
			stmt.setString(1, object.getName());
			stmt.setDouble(2, object.getPrice());
			stmt.setInt(3, object.getId());
			int row = stmt.executeUpdate();
			return row;
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}

		/* Process */
		Bridge.closeBridge();
		return 0;
	}

	public static void main(String[] args) {
		ProductDao dao = new ProductDao();
		System.out.println(dao.getAll());
		System.out.println(dao.get(6));
		dao.delete(7);
		int id = dao.add(new Product(-1, "Operator", 5000));
		System.out.println("id: " + id);
		Product lastProduct = dao.get(id);
		System.out.println("Last Product: " + lastProduct);
		lastProduct.setPrice(100);
		dao.update(lastProduct);
		Product updateProduct = dao.get(id);
		System.out.println("Update Product: " + updateProduct);
		dao.delete(id);
		Product deleteProduct = dao.get(id);
		System.out.println("delete Product : " + deleteProduct);
	}

}
