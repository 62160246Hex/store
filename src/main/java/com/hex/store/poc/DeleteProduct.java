/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hex.store.poc;

import database.Bridge;
import java.sql.*;

/**
 *
 * @author Code.Addict
 */
public class DeleteProduct {

	public static void main(String[] args) {

		Connection bridge = Bridge.createBridge();

		/* Process */
		try {
			String queryStr = "DELETE FROM product WHERE name = ?";
			PreparedStatement stmt = bridge.prepareStatement(queryStr);
			stmt.setString(1, "AK-47");
			int rowChanged = stmt.executeUpdate();
			System.out.printf("RowChanged(%d)", rowChanged);
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}

		/* Process */
		Bridge.closeBridge();
	}
}
