/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hex.store.poc;

import database.Bridge;
import java.sql.*;

/**
 *
 * @author Code.Addict
 */
public class InsertProduct {

	public static void main(String[] args) {
		Connection bridge = Bridge.createBridge();

		try {
			String queryStr = "INSERT INTO product(name, price) VALUES(?, ?)";
			PreparedStatement stmt = bridge.prepareStatement(queryStr);
			stmt.setString(1, "Revolver");
			stmt.setDouble(2, 850.00);
			int row = stmt.executeUpdate();
			ResultSet data = stmt.getGeneratedKeys();
			if (data.next()) {
				System.out.printf("RowChanged(%d)\nId(%d)\n", row, data.getInt(1));
			}
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}

		Bridge.closeBridge();
	}

}
